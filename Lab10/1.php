<?php

function calculateYears($initialAmount, $interestRate, $targetAmount) {
    $years = 0;
    $currentAmount = $initialAmount;

    while ($currentAmount < $targetAmount) {
        $currentAmount += $currentAmount * ($interestRate / 100);
        $years++;
    }

    return $years;
}

$initialDeposit = 10000;
$annualInterestRate = 10;
$targetAmount = 1000000;

$yearsToReachTarget = calculateYears($initialDeposit, $annualInterestRate, $targetAmount);

echo "Через $yearsToReachTarget років на рахунку буде $targetAmount грн.\n";

// Давайте розглянемо випадок, коли хтось кладе гроші в банк щорічно:
$personAge = 18;
$yearsForPerson = calculateYears(0, $annualInterestRate, $targetAmount);

echo "Якщо сьогодні хтось є $personAge років, то він доживе до мільйона через $yearsForPerson років.\n";

?>
