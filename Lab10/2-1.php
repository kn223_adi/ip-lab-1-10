<?php

function findDuplicates($array) {
    $counts = array_count_values($array);

    echo "Повторюються елементи:\n";

    foreach ($counts as $value => $count) {
        if ($count > 1) {
            echo "$value\n";
        }
    }
}

// Приклад масиву
$array = [1, 2, 3, 4, 2, 5, 6, 3, 7, 8, 5];

// Виклик функції для знаходження повторюючихся елементів
findDuplicates($array);

?>
