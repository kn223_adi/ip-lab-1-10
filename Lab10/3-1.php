<?php

// Власна функція для обчислення тангенсу
function my_tg($x) {
    return sin($x) / cos($x);
}

// Власна функція для обчислення факторіалу
function factorial($n) {
    if ($n == 0) {
        return 1;
    } else {
        return $n * factorial($n - 1);
    }
}

// HTML форма для вводу даних
echo '<form method="post" action="">';
echo '<label for="angle">Введіть кут в градусах:</label>';
echo '<input type="text" name="angle" id="angle" required>';
echo '<input type="submit" value="Обчислити">';
echo '</form>';

// Обробка введених даних та виведення таблиці
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $angle = $_POST['angle'];

    echo '<table border="1">';
    echo '<tr><th>Кут (градуси)</th><th>sin</th><th>cos</th><th>tg (вбудована)</th><th>tg (власна)</th><th>x^y</th><th>x!</th></tr>';

    for ($i = 0; $i <= $angle; $i += 10) {
        $radians = deg2rad($i);

        echo '<tr>';
        echo "<td>$i</td>";
        echo '<td>' . round(sin($radians), 4) . '</td>';
        echo '<td>' . round(cos($radians), 4) . '</td>';
        echo '<td>' . round(tan($radians), 4) . '</td>';
        echo '<td>' . round(my_tg($radians), 4) . '</td>';
        echo '<td>' . round(pow($i, $i), 4) . '</td>';
        echo '<td>' . factorial($i) . '</td>';
        echo '</tr>';
    }

    echo '</table>';
}

?>
