<?php

function sumOfDigits($number) {
    // Функція для знаходження суми цифр числа
    return array_sum(str_split($number));
}

function reverseDigits($number) {
    // Функція для отримання числа, отриманого виписуванням в зворотному порядку цифр
    return intval(strrev($number));
}

function rearrangeDigits($number) {
    // Функція для перестановки цифр так, щоб нове число було найбільшим
    $digits = str_split($number);
    rsort($digits); // Сортуємо цифри у зворотньому порядку
    return intval(implode($digits));
}

// Задане тризначне число
$threeDigitNumber = 123;

// Знаходимо суму його цифр
$sum = sumOfDigits($threeDigitNumber);
echo "Сума цифр числа: $sum\n";

// Знаходимо число, отримане виписуванням в зворотному порядку цифр
$reversedNumber = reverseDigits($threeDigitNumber);
echo "Число, отримане в зворотньому порядку: $reversedNumber\n";

// Переставляємо цифри так, щоб нове число було найбільшим
$maxNumber = rearrangeDigits($threeDigitNumber);
echo "Найбільше число, отримане перестановкою цифр: $maxNumber\n";

?>
