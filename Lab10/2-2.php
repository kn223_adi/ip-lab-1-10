<?php

function generateRandomName($type) {
    $prefixes = [
        'Кицька',
        'Пес',
        'Брат',
        'Сестра',
    ];

    $suffixes = [
        'Мурчик',
        'Рекс',
        'Макс',
        'Ліза',
    ];

    $randomPrefix = $prefixes[array_rand($prefixes)];
    $randomSuffix = $suffixes[array_rand($suffixes)];

    return "$randomPrefix $randomSuffix для $type";
}

// Приклад використання для кішки
$randomCatName = generateRandomName('кішки');
echo "Ім'я для кішки: $randomCatName\n";

// Приклад використання для собаки
$randomDogName = generateRandomName('собаки');
echo "Ім'я для собаки: $randomDogName\n";

// Приклад використання для брата
$randomBrotherName = generateRandomName('брата');
echo "Ім'я для брата: $randomBrotherName\n";

// Приклад використання для сестри
$randomSisterName = generateRandomName('сестри');
echo "Ім'я для сестри: $randomSisterName\n";

?>
